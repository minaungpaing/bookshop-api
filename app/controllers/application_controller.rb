class ApplicationController < ActionController::Base
	helper_method :current_user
	helper_method :current_admin
	
	before_action :require_login
 
	private
	 
	  def require_login
	    unless Admin.find_by(id: session[:admin_id])
	      
	      redirect_to root_url, notice: "You must be logged in to access this section"
	    end
	end

	private
	def current_user
		@current_user ||= User.find(session[:user_id]) if session[:user_id]
	end

	def current_admin
		@current_admin ||= Admin.find(session[:admin_id]) if session[:admin_id]
	end
end
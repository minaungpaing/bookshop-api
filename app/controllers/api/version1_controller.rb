class Api::Version1Controller < ApplicationController
	skip_before_action :require_login,only:[:books,:show,:reserve,:search,:r_list,:login,:logout,:register]
	def books
		books = Book.all.order(created_at: :DESC)

		result = {status_code: 200, status_mesaage: 'Books', books: books}
		render json: result.to_json, status: 200
	end

	def show
		books = Book.find(params[:id])		

		result = {status_code: 200, status_mesaage: 'Show Books', books: books}
		render json: result.to_json, status: 200
	end

	def reserve
		books = Book.find(params[:id])		
		if params[:r_user]
			if books.update(r_status:"yes",r_user:params[:r_user])
				result = {status_code: 200, status_mesaage: 'Rserved Users', books: books}
				render json: result.to_json, status: 200
			else 			
				result = {status_code: 422, status_mesaage: 'Rserved Error'}
				render json: result.to_json, status: 200
			end 
		else 
			result = {status_code: 422, status_mesaage: 'No Rserved Users'}
			render json: result.to_json, status: 200
		end		
	end

	def cancel
		books = Book.find(params[:id])		
		if params[:r_user]
			if books.update(r_status:"no",r_user: " ")
				result = {status_code: 200, status_mesaage: 'Rserved Cancel', books: books}
				render json: result.to_json, status: 200
			else			
				render json: {status_code: 422, status_message: 'Reserve Cancel Error!'}
			end 
		else 
			result = {status_code: 422, status_mesaage: 'No Rserved Users'}
			render json: result.to_json, status: 200	
		end
	end
	
	def search
   		books = Book.where("lower(category) LIKE :search", {search: "%#{params[:category].downcase}%"})

   		result = {status_code: 200, status_mesaage: 'Search Books', data: books}
		render json: result.to_json, status: 200
  	end

  	def r_list
    	if params[:r_user]
			list = Book.where(r_user:params[:r_user])

	    	result = {status_code: 200, status_mesaage: 'My Reserved List', data: list}
			render json: result.to_json, status: 200
		else 
			result = {status_code: 422, status_mesaage: 'No Rserved Users list'}
			render json: result.to_json, status: 200	
		end
  	end

  	def login
  		user = User.find_by_email(params[:email])
  		if user && user.authenticate(params[:password])
  			result = {status_code: 200, status_mesaage: 'User Login!',data: user}
			render json: result.to_json, status: 200
  		else
  			result = {status_code: 401, status_mesaage: 'User Login Error!'}
			render json: result.to_json, status: 401
  		end
  	end

  	def logout
  		user = User.find(params[:id])	
  		if user
  			result = {status_code: 200, status_mesaage: 'User logout!'}
			render json: result.to_json, status: 200
  		else
  			result = {status_code: 401, status_mesaage: 'User logout errors!'}
			render json: result.to_json, status: 401
  		end
  	end

  	def register
  		user = User.all
  		if  user.email == params[:email] or user.name == params[:name] 
  			result = {status_code: 401, status_mesaage: 'User email or name is already taken!'}
			render json: result.to_json, status: 401
		else
			user = User.new(user_params)
			if user.save()
				result = {status_code: 200, status_mesaage: 'User Added!'}
				render json: result.to_json, status: 200
			end
  		end
  	# 	user = User.new(user_params)
  	# 	if user.save
  	# 		result = {status_code: 200, status_mesaage: 'User Added!'}
			# 	render json: result.to_json, status: 200
  	# 	else
  	# 		result = {status_code: 401, status_mesaage: 'User email or name is already taken!'}
			# render json: result.to_json, status: 401
  	# 	end
  	end

	private
	def book_params
		params.require(:book).permit(:title,:author,:p_year,:price,:category,:r_status,:img)
  	end

	def user_params
		params.require(:user).permit(:name,:email,:password_digest,:address)
  	end
end
